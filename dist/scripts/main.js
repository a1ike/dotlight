"use strict";

function hideAllError(str) {
  return true;
}

window.onerror = hideAllError;
$(document).ready(function () {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });
  $('.d-nav__toggle-this').on('click', function (e) {
    e.preventDefault();
    $('.d-nav__drop').slideToggle('fast');
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.d-modal').toggle();
  });
  $('.d-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'd-modal__centered') {
      $('.d-modal').hide();
    }
  });
  $('.d-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.d-modal').hide();
  });
  $('.open-modal-order').on('click', function (e) {
    e.preventDefault();
    $('.d-modal-order').toggle();
  });
  $('.d-modal-order .d-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'd-modal__centered') {
      $('.d-modal-order').hide();
    }
  });
  $('.d-modal-order__close').on('click', function (e) {
    e.preventDefault();
    $('.d-modal-order').hide();
  });
  new Swiper('.d-home-projects__cards', {
    navigation: {
      nextEl: '.d-home-projects .swiper-button-next',
      prevEl: '.d-home-projects .swiper-button-prev'
    },
    allowTouchMove: false,
    spaceBetween: 40
  });
  new Swiper('.d-clients__cards', {
    navigation: {
      nextEl: '.d-clients__cards .swiper-button-next',
      prevEl: '.d-clients__cards .swiper-button-prev'
    },
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 2
      },
      1024: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper('.d-papers__cards', {
    navigation: {
      nextEl: '.d-papers .swiper-button-next',
      prevEl: '.d-papers .swiper-button-prev'
    },
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 2
      },
      1024: {
        slidesPerView: 3
      },
      1200: {
        slidesPerView: 4
      }
    }
  });
  new Swiper('.d-done__cards', {
    navigation: {
      nextEl: '.d-done .swiper-button-next',
      prevEl: '.d-done .swiper-button-prev'
    },
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 1
      },
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 2
      }
    }
  });
  new Swiper('.d-done__cards_page', {
    navigation: {
      nextEl: '.d-papers .swiper-button-next',
      prevEl: '.d-papers .swiper-button-prev'
    },
    spaceBetween: 30,
    breakpoints: {
      768: {
        slidesPerView: 1
      },
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 2
      }
    }
  });
  new Swiper('.d-video__cards', {
    navigation: {
      nextEl: '.d-video .swiper-button-next',
      prevEl: '.d-video .swiper-button-prev'
    },
    pagination: {
      el: '.d-video .swiper-pagination'
    }
  });
  new Swiper('.d-more__cards', {
    navigation: {
      nextEl: '.d-more .swiper-button-next',
      prevEl: '.d-more .swiper-button-prev'
    },
    spaceBetween: 0,
    breakpoints: {
      768: {
        slidesPerView: 1
      },
      1024: {
        slidesPerView: 2
      },
      1200: {
        slidesPerView: 3
      }
    }
  });
  $('.d-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');
    $('.d-tabs__header li').removeClass('current');
    $('.d-tabs__content').removeClass('current');
    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });
  var topSwiper = new Swiper('.d-top__cards', {
    fadeEffect: {
      crossFade: true
    },
    effect: 'fade',
    pagination: {
      el: '.d-top__cards .swiper-pagination',
      clickable: true
    },
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    },
    on: {
      init: function init() {
        $('#countSlides').html(this.slides.length);
      }
    }
  });
  topSwiper.on('slideChange', function () {
    $('#currentSlide').html(++topSwiper.realIndex);
    ;
  });
  var galleryThumbs = new Swiper('.d-gallery-thumbs', {
    spaceBetween: 0,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true
  });
  var galleryTop = new Swiper('.d-gallery-top', {
    spaceBetween: 0,
    thumbs: {
      swiper: galleryThumbs
    }
  });
  var gallery2Thumbs = new Swiper('.d-good-thumbs', {
    direction: 'vertical',
    spaceBetween: 0,
    slidesPerView: 5,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true
  });
  var gallery2Top = new Swiper('.d-good-top', {
    spaceBetween: 0,
    thumbs: {
      swiper: gallery2Thumbs
    }
  });
});
//# sourceMappingURL=main.js.map
